# Tshark

### Extracting encoded DNS queries  
Yes, Wireshark is awesome. However, sometimes the command line version tshark will be the way to go. I find myself using tshark mostly when analyzing traffic that requires
me to extract files or text. A great example of this is in the analysis of encoded DNS TXT queries which attackers will often use as a method of infiltration.

1.  The first thing to do is isolate the query response, this is done in tshark using the -Y flag which enables you to assign filters to your data.  
`tshark -r capture.pcap -Y 'dns.flags == 0x8400` this will isolate all the DNS packets in your pcap file.  

2.  Next, you want to extract the contents of the packets, you can do this by using fields. We are interested in the DNS TXT record.
`tshark -r capture.pcap -Y 'dns.flags == 0x8400' -T fields -e dns.txt'` this tells tshark we only want txt responses.  

3. You should now have a wall of encoded text in front of you. Based on the encoding you can use the following  
`tshark -r capture.pcap -Y 'dns.flags == 0x8400' -T fields -e dns.txt > dnstxt.txt`    

You can then decode by doing something like the following. You can, of course, redirect the output to a file.  
* hex: `cat dnstxt.txt | xxd -r -p`  
* base64: `cat dnstxt.txt | openssl base64 -d`  


### Extracting encoded information in query strings  
Another spot where I turn to tshark is when I see long query strings broken up across multiple packets. This is easy to spot in wireshark. You will see lines such as  
` HTTP    1000        GET /example.php?segment=7bcf270d....`  
` HTTP    285         HTTP/1.1 200 OK (text/html)`  
` HTTP    1000        GET /example.php?segment=8cd2321e....`    
` HTTP    285         HTTP/1.1 200 OK (text/html)`  

Very easy to spot the repeated query string lines here. If you were to follow the stream on one of these packets you may see something like this.

`GET /example.php?segment=3946344432443544363838433e3c3037344643373643446...&num=1 HTTP/1.1`  

Notice that the encoded string lies between 'segment' and 'num'.  

1. Over in tshark you can easily extract all the packets which contain a parameter
`tshark -r capture.pcap -Y 'http.request.uri contains segment'`  

This will output every packet that contains this parameter. We are after the encoded portion between the segment and num parameters, let's get it.

2. You will want to extract and combine these segments together. The following will target the query parameter using a field.
`tshark -r capture.pcap -Y 'http.request.uri contains segment' -T fields -e http.request.uri.query.parameter`  

This will remove the IP address information, length, protocol info etc, and return just query parameters and the encoded data between them.

Original: `GET /example.php?segment=3946344432443544363838433e3c3037344643373643446...&num=1 HTTP/1.1`  
Current: `segment=3946344432443544363838433e3c3037344643373643446...&num=1`  

This will allow us to easily extract only the encoded data.

3. The best way to do this is by piping in an awk command. The following is the tshark command and an explanation of what the awk command is doing.
`tshark -r capture.pcap -Y 'http.requests.uri contains segment' -T fields -e http.request.uri.query.parameter | awk -F '[,=]' '{print $2}'`  

Here, I am using the awk option `-F` and providing `[,=]` as an argument. This is defining an inclusive field seperator. Consider `segment=3c3c2f53697a652032362f526f6f742031203020522f496e666f20313,num=112`,
the `awk -F '[,=]'` command will return `=3c3c2f53697a652032362f526f6f742031203020522f496e666f20313,`.  Notice its return everything between the `=` and `,` including the `=` and `,`.
We use `'{print $2}'` to target the second column in which is our encoded string.  

`  C1                            C2                                 C3`     
` | = | 3c3c2f53697a652032362f526f6f742031203020522f496e666f20313 | , |`  

4. Depending on the encoding used, pipe a decoder and redirect the output to a file. For hex encoding you can do the following.  
`tshark -r capture.pcap -Y 'http.request.uri contains segment' -T fields -e http.request.uri.query.parameter | awk -F '[,=]' '{print $2}' | xxd -r -ps > /tmp/obfuscated.file`

5. You can then use the file command to tell you what sort of file you are dealing with.  
`# file /tmp/obfuscated.file`  
`/tmp/obfuscated.file: PDF document, version 1.5`